package ch.ipt.oauth2.playground.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ch.ipt.oauth2.playground.model.TokenData;
import ch.ipt.oauth2.playground.service.ConversationService;
import ch.ipt.oauth2.playground.service.TokenWalletService;

/**
 * Callback end point invoked from the OAuth Authorization endpoint with the response of the  authorization request. If authorization code
 * is returned for the correct user's session the parsed authorization code is stored in-memory and the user is redirected to the main (index.html) page. 
 * 
 * Note: The end point is called within a redirect from the browser and NOT directly by the OAuth server. This is why also localhost is acceptable for local development.
 *
 */
@Component
@Path("/")
public class Callback {
	
	private static final Logger log = Logger.getLogger(Callback.class);
	
	@Autowired
	private ConversationService conversationService;

	@Autowired
	private TokenWalletService tokenWalletService;
	
	@Context
	private HttpServletResponse response;
	
	@Context
	private HttpServletRequest request;
	
    @GET
    @Path("callback")
	public void processCallback(@QueryParam("code") String code, @QueryParam("state") String state, @QueryParam("error") String error) throws IOException {
    	if(log.isDebugEnabled())log.debug("========================================================");
		if(log.isDebugEnabled())log.debug("Step 2: Got redirect:");
		// OAuth Server returns a code that can be exchanged for a access token
		if(log.isDebugEnabled())log.debug("code " + code);
		if(log.isDebugEnabled())log.debug("state " + state);
		if(log.isDebugEnabled())log.debug("error " + error);
		
		// if the user denied access, we get back an error, ex
		// error=access_denied&state=session%3Dpotatoes
				
		if (error != null) {
			response.getWriter().println(error);
			return;
		}
		
		String sessionId = request.getSession().getId();
		
		if (!sessionId.equals(state)) {
			response.getWriter().println("sessioId mismatch try again");
			return;
		}
		
		conversationService.saveLastResponse(request.toString());
		
		//save returned code
		TokenData tokenData = new TokenData();
		tokenData.setCode(code);
		tokenWalletService.put(tokenData);
		
    	response.sendRedirect("/index.html");
	}
}