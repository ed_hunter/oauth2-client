package ch.ipt.oauth2.playground.controller;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ch.ipt.oauth2.playground.OAuthClientException;
import ch.ipt.oauth2.playground.model.ApiInvokeRequest;
import ch.ipt.oauth2.playground.model.OAuthData;
import ch.ipt.oauth2.playground.model.TokenData;
import ch.ipt.oauth2.playground.service.ConversationService;
import ch.ipt.oauth2.playground.service.HttpService;
import ch.ipt.oauth2.playground.service.OAuthWalletService;
import ch.ipt.oauth2.playground.service.TokenWalletService;

/**
 * RESTful web services exposed by the java back-end.
 * 
 * This class provides the interface and the "glue" between the AngularJS front-end 
 * and the java back-end.
 *
 */
@Component
@Path("/api")
public class RestApi {
	private static final Logger log = Logger.getLogger(RestApi.class);
	
	@Value("${oauth2.callback.url}")
	private String callbackUrl;
	
	@Context
	private HttpServletRequest request;
	
	@Autowired
	private HttpService httpService;
	
	@Autowired
	private ConversationService conversationService;
	
	@Autowired
	private OAuthWalletService oauthWalletService;
	
	@Autowired
	private TokenWalletService tokenWalletService;
	
	
    /**
     * Used on every step.
     * @return The last saved response for the user's session  
     */
    @GET
    @Path("last-request")
    @Produces(MediaType.TEXT_PLAIN)
    public String getLastRequest() {
        return conversationService.getLastRequest();
    }
    
    /**
     * Used on every step.
     * @return The last saved request for the user's session  
     */    
    @GET
    @Path("last-response")
    @Produces(MediaType.TEXT_PLAIN)
    public String getLastResponse() {
        return conversationService.getLastRepsponse();
    }
    
    /**
     * Stores the provided  {@link OAuthData} in the user's session. Used in Step 1.
     * @return appends the finalUrl appended with state parameter containing the user's session. This URL can then be used for redirection for requesting authorization token.
     */
    @POST
    @Path("save-oauth-data")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN) 
    public String saveOauthData(OAuthData data){
    	if (log.isDebugEnabled()){
    		log.debug("saveOauthData");
    		Gson gson = new GsonBuilder().setPrettyPrinting().create();
	    	String jsonRequest= gson.toJson(data);
    		log.debug(jsonRequest);
    	}
    		
    	oauthWalletService.put(data);
    	StringBuilder sb = new StringBuilder(data.getUrlToBeInvoked());
    	String finalUrl = sb.append("&state=").append(getSessionId()).toString();
    	
    	conversationService.saveLastRequest(finalUrl);
    	conversationService.saveLastResponse("redirect");

    	return finalUrl;
    }
    
    /**
     * Used in Step 1.
     * @return the {@link OAuthData} for the user's session saved by {@link RestApi#saveOauthData(OAuthData)}. If saveOauthData was not called,
     * a new object is initialized containing the callback URL from the client.properties. 
     */
    @GET
    @Path("oauth-data")
    @Produces(MediaType.APPLICATION_JSON)
    public OAuthData getOAuthData() {
    	OAuthData data  = oauthWalletService.get();
    	if (data == null){
    		data = new OAuthData();
    		//we need to initialize the callback_url on first login
    		data.setCallbackUrl(callbackUrl);
    	}
    	return data;
    }
    
    /**
     * Used in Step 2 & 3
     * @return {@link TokenData} stored for the user's session.
     */
    @GET
    @Path("token-data")
    @Produces(MediaType.APPLICATION_JSON)
    public TokenData getTokenData() {
        return tokenWalletService.get();
    }
    
    /**
     * Sends request to the OAuth server's token endpoint to exchange an available authorization token for an acces token. Used in Step 2.
     * @return {@link TokenData} parsed response from the token service.
     */
    @GET
    @Path("exchange-token")
    @Produces(MediaType.APPLICATION_JSON)
    public TokenData exchangeToken() {
    	OAuthData data = oauthWalletService.get();
    	TokenData tokenData  = tokenWalletService.get();
    	if(tokenData == null){
    		//authentication token was not yet requested or is missing.
    		return null;
    	}
    	
    	Map<String, String> paramMap = new HashMap<>();
		paramMap.put("grant_type", "authorization_code");
		paramMap.put("client_id", data.getClientId());
		paramMap.put("client_secret", data.getClientSecret());
		paramMap.put("redirect_uri", data.getCallbackUrl());
		paramMap.put("code", tokenData.getCode());
		
		getAccessToken(paramMap, data, tokenData);
		
    	return tokenData;
    }
    
    /**
     * Sends request to the OAuth server's token endpoint to refresh an existing access token. Used in Step 2.
     * @return {@link TokenData} parsed response from the token service.
     */
    @GET
    @Path("refresh-token")
    @Produces(MediaType.APPLICATION_JSON)
    public TokenData refreshToken()  {
    	OAuthData data = oauthWalletService.get();
    	TokenData tokenData  = tokenWalletService.get();
    	if(tokenData == null){
    		//authentication token was not yet requested or is missing.
    		return null;
    	}
    	Map<String, String> paramMap = new HashMap<>();
		paramMap.put("grant_type", "refresh_token");
		paramMap.put("client_id", data.getClientId());
		paramMap.put("client_secret", data.getClientSecret());
		paramMap.put("refresh_token", tokenData.getRefreshToken());
		
		getAccessToken(paramMap, data, tokenData);
		
    	return tokenData;
    }
    
    /**
     * Triggers a GET request for the provided parameters and stores the response for the user's session. Used in Step 3.
     * @param request {@link ApiInvokeRequest} form data
     */
    @POST
    @Path("invoke-api-get")
    @Consumes(MediaType.APPLICATION_JSON)
    public void invokeApiGet(ApiInvokeRequest request)  {
    	if(log.isDebugEnabled())log.debug("URL to be invoked: '" + request.getUrl() + "'");
    	
    	Map<String, String> headers = parseHeaders(request.getHeadersString());
    	
    	//invoke and store request/response in memory
    	try {
    		httpService.get(request.getUrl(), headers);
		} catch (OAuthClientException e) {
			log.error(e);
		}
    }
    
    /**
     * Triggers a POST request for the provided parameters and stores the response for the user's session. Used in Step 3.
     * @param request {@link ApiInvokeRequest} form data
     */
    @POST
    @Path("invoke-api-post")
    @Consumes(MediaType.APPLICATION_JSON)
    public void invokeApiPost(ApiInvokeRequest request) {
    	if(log.isDebugEnabled())log.debug("URL to be invoked: '" + request.getUrl() + "' with formParameters '" + request.getJsonString() + "'");
    	
    	Map<String, String> headers = parseHeaders(request.getHeadersString());
    	
    	//invoke and store request/response in memory
		try {
			httpService.post(request.getUrl(), headers, request.getJsonString());

		} catch (OAuthClientException e) {
			log.error(e);
		}
    }
    
    private static Map<String, String> parseHeaders(String headersString){
    	Map<String, String> headersMap = new HashMap<>();
    	//this should be validated
    	for (String nvps: headersString.split(",")){
    		String[] nvp = nvps.split("=");
    		headersMap.put(nvp[0].trim(), nvp[1].trim());
    	}
    	return Collections.unmodifiableMap(headersMap);
    }
    
    private void getAccessToken(Map<String, String> paramMap, OAuthData data, TokenData tokenData) {
		
		// get the access token by post to the token service
		String body;
		try {
			body = httpService.post(data.getTokenServerUrl(), Collections.unmodifiableMap(paramMap));
		} catch (OAuthClientException e) {
			log.error(e);
			//don't modify tokenData
			return;
		}
		JSONObject jsonObject = null;
		
		// get the access token and refresh token from json
		try {
			jsonObject = (JSONObject) new JSONParser().parse(body);
		} catch (ParseException e) {
			throw new RuntimeException("Unable to parse json " + body);
		}
		
		if (log.isDebugEnabled()){
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			
			log.debug("Access Token Response: ");
			log.debug(gson.toJson(jsonObject));
			log.debug("========================================================");
		}
		
		tokenData.setAccessToken((String)jsonObject.get("access_token"));
		String refreshToken = (String) jsonObject.get("refresh_token");
		if(refreshToken!=null && !"".equals(refreshToken)){
			tokenData.setRefreshToken((String) jsonObject.get("refresh_token"));
		}
		//save and return data
		Object expiresIn =jsonObject.get("expires_in");
		if (expiresIn != null) {
			try {
				Long expiresInLong;
				if (expiresIn instanceof Long) {
					expiresInLong = (Long) expiresIn;
				} else {
					expiresInLong = Long.valueOf((String) expiresIn);
				}

				long expiritionMillis = System.currentTimeMillis() + expiresInLong * 1000;
				tokenData.setExpirationDate(new Date(expiritionMillis).toString());
			} catch (Exception e) {
				log.warn("Cannot parse expires_in: " + e.getMessage(), e);
			}
		}
    }
    
    private String getSessionId(){
    	log.info("SessionId: "  + request.getSession().getId());
    	return request.getSession().getId();
    }
}
