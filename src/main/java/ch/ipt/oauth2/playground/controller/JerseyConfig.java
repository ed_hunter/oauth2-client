package ch.ipt.oauth2.playground.controller;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
@ApplicationPath("/web")
public class JerseyConfig extends ResourceConfig{
	public JerseyConfig() {
		register(RestApi.class);
		register(Callback.class);
	}
}
