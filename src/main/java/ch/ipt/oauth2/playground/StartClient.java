package ch.ipt.oauth2.playground;


import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * Main class and entry point for the OAuth2 client web app.
 *
 */
@SpringBootApplication
public class StartClient extends SpringBootServletInitializer{
	
	/**
	 * Starts the web app
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		
		new StartClient()
			.configure(new SpringApplicationBuilder(StartClient.class))
			.run(args);
	}
}
