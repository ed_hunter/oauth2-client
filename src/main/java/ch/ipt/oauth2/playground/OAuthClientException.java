package ch.ipt.oauth2.playground;

/**
 * Custom exception for the OAuth2 client web app.
 *
 */
public class OAuthClientException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2303949126156759901L;
	
	public OAuthClientException(String string) {
		super(string);
	}
	
	public OAuthClientException() {
		super();
	}

}
