package ch.ipt.oauth2.playground.model;

/**
 * Java form bean for the JSON Object oauthData.
 * 
 * Used in Step 1.
 *
 */
public class OAuthData {
	
	private String clientId;
	private String clientSecret;
	private String oauthServerUrl;
	private String tokenServerUrl;
	private String callbackUrl;
	private String state;
	private String scope;
	private String urlToBeInvoked;

	public OAuthData() {
		super();
	}
	
	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public String getOauthServerUrl() {
		return oauthServerUrl;
	}

	public void setOauthServerUrl(String oauthServerUrl) {
		this.oauthServerUrl = oauthServerUrl;
	}

	public String getTokenServerUrl() {
		return tokenServerUrl;
	}

	public void setTokenServerUrl(String tokenServerUrl) {
		this.tokenServerUrl = tokenServerUrl;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getUrlToBeInvoked() {
		return urlToBeInvoked;
	}

	public void setUrlToBeInvoked(String urlToBeInvoked) {
		this.urlToBeInvoked = urlToBeInvoked;
	}
	
}
