package ch.ipt.oauth2.playground.model;

/**
 * Java form bean for the JSON Object oauthData.
 * 
 * Used in Step 2.
 *
 */
public class TokenData {
	
	private String code;
	private String refreshToken;
	private String accessToken;
	private String expirationDate;
	
	public String getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getRefreshToken() {
		return refreshToken;
	}
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

}
