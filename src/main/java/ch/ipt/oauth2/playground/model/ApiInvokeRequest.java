package ch.ipt.oauth2.playground.model;

/**
 * Java form bean for the JSON Object apiInvokeRequest
 * 
 * Used in Step 3.
 *
 */
public class ApiInvokeRequest {
	private String url;
	private String jsonString;
	private String headersString;
	
	public String getHeadersString() {
		return headersString;
	}
	public void setHeadersString(String headersString) {
		this.headersString = headersString;
	}
	public String getJsonString() {
		return jsonString;
	}
	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

}
