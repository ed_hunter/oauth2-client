package ch.ipt.oauth2.playground.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import ch.ipt.oauth2.playground.OAuthClientException;

/**
 * Helper class for HTTP GET/POST requests using the apache http client library.
 *
 */
@Service
@SessionScope
public class HttpServiceImpl implements HttpService{
	
	private static final Logger log = Logger.getLogger(HttpServiceImpl.class);
	
	@Autowired
	private ConversationService conversationService;
	
	public String get(String url, Map<String, String> headers) throws OAuthClientException {
		HttpGet request = new HttpGet(url);
		
		setHeaders(request, headers);

		conversationService.saveLastRequest(request.getRequestLine().toString()+"\nHeaders:"+ printHeaders(request.getAllHeaders()) );
		return execute(request);
	}
	
	
	public String post(String url, Map<String, String> headers, String json) throws OAuthClientException {	
		HttpPost request = new HttpPost(url);
		
		setHeaders(request, headers);
		
		try {
			StringEntity se = new StringEntity(json, ContentType.create("application/json"));
			request.setEntity(se);
			
			conversationService.saveLastRequest(request.getRequestLine().toString() + "\nHeaders: "+ printHeaders(request.getAllHeaders()) +" \nbody: " + EntityUtils.toString(se));
		} catch (Exception e) {
			log.error(e);
			conversationService.saveLastRequest(e.getMessage());
			//no response will be sent
			conversationService.saveLastResponse(null);
		}
		
		return execute(request);
	}
	
	private void setHeaders(HttpRequestBase request, Map<String, String> headers){
		if(headers!= null && headers.size() > 0 && request != null){
			for (String key : headers.keySet()){
				request.addHeader(key, headers.get(key));
			}
		}
	}
	
	// makes a POST request to url with form parameters and returns body as a string
	public String post(String url, Map<String,String> formParameters) throws OAuthClientException {	
		HttpPost request = new HttpPost(url);
		
		List <NameValuePair> nvps = new ArrayList <NameValuePair>();
		
		for (String key : formParameters.keySet()) {
			NameValuePair nvp = new BasicNameValuePair(key, formParameters.get(key));
			nvps.add(nvp);	
		}

		try {
			UrlEncodedFormEntity uefe = new UrlEncodedFormEntity(nvps);
			request.setEntity(uefe);
			String body = EntityUtils.toString(request.getEntity());
			
			conversationService.saveLastRequest(request.getRequestLine().toString()+ "\nHeaders: "+ printHeaders(request.getAllHeaders()) +" \nbody: " + body);
		} catch (Exception e) {
			log.error(e);
			conversationService.saveLastRequest(e.getMessage());
			//no response will be sent
			conversationService.saveLastResponse(null);
		}
		
		return execute(request);
	}
	
	// makes request and checks response code for 200
	private String  execute(HttpRequestBase request) throws OAuthClientException  {
		
		String body = null;
		try {

			HttpClient httpClient = HttpClientBuilder.create().build();

			
			HttpResponse response = httpClient.execute(request);
			HttpEntity entity = response.getEntity();
		    body = EntityUtils.toString(entity);
		    
		    conversationService.saveLastResponse(response.getStatusLine() + "\nHeaders:"+ printHeaders(response.getAllHeaders()) + "\nbody: " + body);
		    
		    if (response.getStatusLine().getStatusCode() != 200){
		    	throw new OAuthClientException("Got error code " + response.getStatusLine().getStatusCode());
		    }
		    

		} catch (IOException e) {
			log.error(e);
			//in case of org.apache.http.client.ClientProtocolException 
			//e.getMessage() is null
			conversationService.saveLastResponse(e.getMessage()!=null?e.getMessage():e.toString());
			throw new OAuthClientException(e.getMessage());
		}
	    return body;
	}
	
	private String printHeaders(Header[] headers){
		StringBuilder sb = new StringBuilder();
		
		if(headers!=null){
			for (Header header : headers){
				sb.append(header).append(' ');
			}
		}
		return sb.toString();
	}
}
