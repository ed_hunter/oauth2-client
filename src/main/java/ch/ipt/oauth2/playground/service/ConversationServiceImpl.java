package ch.ipt.oauth2.playground.service;

import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;


@Service
@SessionScope
public class ConversationServiceImpl implements ConversationService{
	private String lastRequest = "";
	private String lastResponse = "";
	
	
	public String getLastRepsponse(){
		return lastResponse;
	}
	
	public String getLastRequest(){
		return lastRequest;
	}
	
	public void saveLastResponse(String response){
		 lastResponse = response;
	}
	
	public void saveLastRequest(String request){
		 lastRequest = request;
	}
}
