package ch.ipt.oauth2.playground.service;

import ch.ipt.oauth2.playground.model.OAuthData;

/**
 * In-memory storage for {@link OAuthData} mapped to the user's session.
 *
 */
public interface OAuthWalletService {
	
	public void put(OAuthData value);
	
	public OAuthData get();

}
