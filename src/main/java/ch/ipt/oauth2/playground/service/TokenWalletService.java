package ch.ipt.oauth2.playground.service;

import ch.ipt.oauth2.playground.model.TokenData;

/**
 * In-memory storage for {@link TokenData} mapped to the user's session.
 *
 */
public interface TokenWalletService {
	
	public void put(TokenData value);
	
	public TokenData get();

}
