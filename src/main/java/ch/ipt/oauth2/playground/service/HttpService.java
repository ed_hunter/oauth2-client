package ch.ipt.oauth2.playground.service;

import java.util.Map;

import ch.ipt.oauth2.playground.OAuthClientException;

public interface HttpService {
	
		/**
		 * makes a GET request to url with headers and returns body as a string. Will save the request/response for the given sessionId
		 * @param url
		 * @param headers
		 * @return
		 * @throws OAuthClientException
		 */
		public String get(String url, Map<String, String> headers) throws OAuthClientException;
		
		/**
		 * makes a POST request to url with headers, json String and returns body as a string. Will save the request/response for the given sessionId
		 * @param url
		 * @param headers
		 * @param json
		 * @return
		 */
		public String post(String url, Map<String, String> headers, String json) throws OAuthClientException;
		
		/**
		 * makes a POST request to url with form parameters and returns body as a string
		 * @param url
		 * @param formParameters
		 * @return
		 * @throws OAuthClientException
		 */
		public String post(String url, Map<String,String> formParameters) throws OAuthClientException;

}
