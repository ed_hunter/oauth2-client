package ch.ipt.oauth2.playground.service;

import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import ch.ipt.oauth2.playground.model.OAuthData;

@Service
@SessionScope
public class OAuthWalletServiceImpl implements OAuthWalletService{
	private  OAuthData data; 
	
	public void put(OAuthData value){
		data = value ;
	}
	
	public OAuthData get(){
		return data;
	}

}
