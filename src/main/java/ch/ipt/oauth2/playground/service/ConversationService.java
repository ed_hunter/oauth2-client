package ch.ipt.oauth2.playground.service;

/**
 * In-memory storage for the last request/response mapped to the user's session.
 *
 */
public interface ConversationService {
	
	public  String getLastRepsponse();
	
	public  String getLastRequest();
	
	public  void saveLastResponse(String response);
	
	public  void saveLastRequest(String request);

}
