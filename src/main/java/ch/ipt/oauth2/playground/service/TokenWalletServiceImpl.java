package ch.ipt.oauth2.playground.service;

import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import ch.ipt.oauth2.playground.model.TokenData;

@Service
@SessionScope
public class TokenWalletServiceImpl implements TokenWalletService {
	private TokenData data; 
	
	public void put( TokenData value){
		data = value;
	}
	
	public TokenData get(){
		return data;
	}
}
