(function(){
    var app = angular.module('client-directives', []);

    app.directive("navTabs", function() {
      return {
        restrict: "E",
        templateUrl: "partials/nav-tabs.html",
        controller: function() {
          this.tab = 1;

          this.isSet = function(checkTab) {
            return this.tab === checkTab;
          };

          this.setTab = function(activeTab) {
            this.tab = activeTab;
          };
        },
        controllerAs: "tab"
      };
    });

    app.directive("requestView", function() {
      return {
        restrict: "E",
        templateUrl: "partials/request-view.html"
      };
    });

    app.directive("responseView", function() {
      return {
        restrict: "E",
        templateUrl: "partials/response-view.html"
      };
    });

    app.directive("oauthTab", function() {
      return {
        restrict: "E",
        templateUrl: "partials/oauth-tab.html",
      };
    });

    app.directive("accessTab", function() {
      return {
        restrict: "E",
        templateUrl: "partials/access-tab.html",
      };
    });

    app.directive("apiTab", function() {
      return {
        restrict: "E",
        templateUrl: "partials/api-tab.html",
      };
    });

    app.directive("validateHeaders", function(){
          // requires an isloated model
          return {
           // restrict to an attribute type.
           restrict: 'A',
          // element must have ng-model attribute.
           require: 'ngModel',
           link: function(scope, ele, attrs, ctrl){

              // add a parser that will process each time the value is
              // parsed into the model when the user updates it.
              ctrl.$parsers.unshift(function(value) {
                
                if(value){
                  var valid = false;
                  avps = value.split(',');
                  for (var x in avps){
                    
                    header= avps[x].split('=');
                    if (header.length == 2 && header[0] && header[1]){
                      valid = true;
                    } else {
                      valid = false;
                      break;
                    }
                  }
                }
                // if it's valid, return the value to the model,
                // otherwise return undefined.
                return valid ? value : undefined;
              });

           }
          }
    });

  })();


