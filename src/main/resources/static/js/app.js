(function() {
  var app = angular.module('oauthClient', ['client-directives']);

  app.controller('MainController',['$http', function($http){
    var main = this;
    main.conversation={};
    main.conversation.request='';
    main.conversation.request='';
    
    this.refreshLastRequest = function() {
      $http.get('/web/api/last-request').success(function(data){
          main.conversation.request = data;
      });
    };
    
    this.refreshLastResponse = function() {
      $http.get('/web/api/last-response').success(function(data){
          main.conversation.response = data;
      });
    };

    this.refreshLastRequest();
    this.refreshLastResponse();

  }]);

  app.controller('OauthTabController', ['$http',  '$scope', '$window',  function($http, $scope, $window) {
    $scope.oauthData={};

    $scope.loadOauthData = function() {
      $http.get('/web/api/oauth-data').success(function(oauthData){
          $scope.oauthData = oauthData;
          $scope.updateUrlToBeInvoked();
      });
    };
    
    $scope.updateUrlToBeInvoked = function() {
      if ($scope.oauthData.oauthServerUrl){
        $scope.oauthData.urlToBeInvoked = $scope.oauthData.oauthServerUrl
                                          .concat('?client_id=').concat($scope.oauthData.clientId)
                                          .concat('&response_type=code')
                                          .concat('&redirect_uri=').concat($scope.oauthData.callbackUrl);
        if($scope.oauthData.scope){
          $scope.oauthData.urlToBeInvoked  = $scope.oauthData.urlToBeInvoked
                                            .concat('&scope=').concat($scope.oauthData.scope);
        }
                                  
      } else {
        $scope.oauthData.urlToBeInvoked  = '';
      }
      
    };
    //Form Init;
    $scope.loadOauthData();
    

    $scope.requestAuthorizationToken = function(main) {
      $http.post('/web/api/save-oauth-data', $scope.oauthData).success(function(urlToBeInvokedWithState){
              main.refreshLastRequest();
              main.refreshLastResponse();
              //redirect to OAuth Server
              $window.location.href=urlToBeInvokedWithState;
    });
    };

  }]);

  app.controller('AccessTabController',['$http', function($http){
    
    var ctrl = this;
    ctrl.tokenData  = {};
    
    this.loadTokenData = function() {
      $http.get('/web/api/token-data').success(function(tokenData){
          ctrl.tokenData = tokenData;
      });
    };

    //Form Init;
    this.loadTokenData();

    this.exchangeToken = function(main) {

      $http.get('/web/api/exchange-token')
        .success(function(tokenData){
              
        ctrl.tokenData = tokenData;
        main.refreshLastRequest();
        main.refreshLastResponse();

      }).error(function(tokenData){
              
        main.refreshLastRequest();
        main.refreshLastResponse();

      });
    };

    this.refreshToken = function(main) {

      $http.get('/web/api/refresh-token')
        .success(function(tokenData){
              
        ctrl.tokenData = tokenData;
        main.refreshLastRequest();
        main.refreshLastResponse();

      }).error(function(tokenData){
              
        main.refreshLastRequest();
        main.refreshLastResponse();

      });
    };

  }]);

  app.controller('ApiTabController',['$http', '$scope', function($http, $scope){
     
    $scope.method  = 'GET';
    $scope.accessToken = '';
    $scope.headers  = '';
    $scope.apiUrl  = '';
    $scope.jsonString='';

    $scope.updateHeaders = function() {
      if($scope.accessToken != null && $scope.accessToken != ''){
        $scope.headers = 'Authorization=Bearer '.concat($scope.accessToken);
      }
    };

    $scope.loadTokenData = function() {
      $http.get('/web/api/token-data').success(function(tokenData){
        var oldAccessToken = $scope.accessToken;

        if(tokenData != null && tokenData != ''){
          $scope.accessToken = tokenData.accessToken;
        }
        if(oldAccessToken != $scope.accessToken){
          $scope.updateHeaders();
        }
        
      });
    };
    //Form Init;
    $scope.loadTokenData();

    $scope.invoke = function(main) {
      var request = {};
      request.url = $scope.apiUrl;
      request.jsonString = $scope.jsonString;
      request.headersString = $scope.headers;

      if($scope.method == 'GET'){
        
        $http.post('/web/api/invoke-api-get', request)
          .success(function(){

          main.refreshLastRequest();
          main.refreshLastResponse();

        }).error(function(response){
          alert(response);
          main.refreshLastRequest();
          main.refreshLastResponse();

        });

      } else if ($scope.method == 'POST') {

        $http.post('/web/api/invoke-api-post', request)
          .success(function(){

          main.refreshLastRequest();
          main.refreshLastResponse();

        }).error(function(response){
          alert(response);
          main.refreshLastRequest();
          main.refreshLastResponse();

        });

      }

    };
  }]);

})();
