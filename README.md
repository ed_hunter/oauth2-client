# oauth2-client
Sample Application that demonstrates the OAuth2 authorization code flow

Technologies used:
* Spring Boot 1.5.3 with Jetty embedded container & Jersey REST api
* Angular JS 1.5.0

## OpenShift deployment
Based on the spring-cloud-kubernetes
[example](https://access.redhat.com/documentation/en-us/red_hat_jboss_fuse/6.3/html/fuse_integration_services_2.0_for_openshift/kube-spring-boot)  

### 1. Login  
*oc login https://console.starter-us-east-1.openshift.com*  
### 2. Create Project  
*oc new-project kge-oauth2-client*  
**Note:** Change kge with unique prefix  
Clean up: *oc delete project kge-oauth2-client*

### 3. Create application.properties
Create **application.properties** file on your local file system with following content:
   
*oauth2.callback.url=https://oauth2-client-kge-oauth2-client.1d35.starter-us-east-1.openshiftapps.com/web/callback*  
  
**Note:** Change the URL according to your OpenShift environment
### 4. Create ConfigMap from application.properties
*oc create configmap oauth2-client-config-map --from-file=application.properties*

### 5. Add view permission to the OpenShift serviceaccount
*oc policy add-role-to-user view --serviceaccount=default*

### 6. Create & Deploy App
*oc new-app registry.access.redhat.com/redhat-openjdk-18/openjdk18-openshift~https://github.com/ipt-konstantin/oauth2-client*

### 7. Add external route
*oc create route passthrough oauth2-client --service=oauth2-client --port=8080-tcp --hostname=oauth2-client-kge-oauth2-client.1d35.starter-us-east-1.openshiftapps.com*  

**Note:** The hostname is the same that is configured in application.properties. If you do not know the value, run the command without hostname parameter and copy the value generated in the web console.